import TestClass, { TestInterface, TestInterface2 } from './types';

const testVal = 1;
function testFunction() {
  return testVal + 1;
}
const testIObj: TestInterface = { p1: 'p1', p2: 2 };
const testIObj2: TestInterface2 = { p1: testIObj, p2: 'TestType' };

const testClassObj = new TestClass(testIObj, testIObj2);