
export interface TestInterface {
  p1: string;
  p2: number;
}

export type TestType = 'TestType';

export interface TestInterface2 {
  p1: TestInterface;
  p2: TestType;
}

export default class TestClass {
  p1: TestInterface;
  p2: TestInterface2;

  constructor(_p1: TestInterface, _p2: TestInterface2) {
    this.p1 = _p1;
    this.p2 = _p2;
  }
}

