
describe('Frontend Test', () => {
  beforeAll(async () => {
    await page.goto('https://localhost:3000')
  })

  it('should be titled "React App"', async () => {
    await expect(page.title()).resolves.toMatch('React App');
  });
})