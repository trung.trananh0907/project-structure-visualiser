import React, { useEffect, useRef, useState } from 'react';
import {
  amber,
  blue,
  blueGrey,
  brown,
  common,
  cyan,
  deepOrange,
  deepPurple,
  green,
  grey,
  indigo,
  lightBlue,
  lightGreen,
  lime,
  orange,
  pink,
  purple,
  red,
  teal,
  yellow
} from '@material-ui/core/colors';

import { makeStyles } from '@material-ui/core';
import { DataContext } from '../App';

import cytoscape from 'cytoscape';
import fcose from 'cytoscape-fcose';
import coseBilkent from 'cytoscape-cose-bilkent';
// import cytoscapeAllPaths from 'cytoscape-all-paths';
import viewUtilities from 'cytoscape-view-utilities';
import contextMenus from 'cytoscape-context-menus';
import avsdf from 'cytoscape-avsdf';
import { get, shortestPaths } from '../crud';

cytoscape.use(fcose);
cytoscape.use(coseBilkent);
// cytoscape.use(cytoscapeAllPaths);
cytoscape.use(viewUtilities);
cytoscape.use(contextMenus);
cytoscape.use(avsdf);

const useStyles = makeStyles(() => ({
  graph: {
    width: '100%',
    height: '100%',
    backgroundColor: '#fff',
  },
  noGraph: {
    width: '100%',
    height: '100%',
    position: 'absolute',
    left: 0,
    top: 0,
    zIndex: 10,
    display: 'flex',
    align: 'center',
    justifyContent: 'center'
  }
}))

function usePrevious(value) {
  const ref = useRef();
  useEffect(() => {
    ref.current = value;
  });
  return ref.current;
}

export const entityColors = [
  amber,
  blue,
  blueGrey,
  brown,
  cyan,
  deepOrange,
  deepPurple,
  green,
  grey,
  indigo,
  lime,
  red,
  teal,
  yellow,
  common
];

export const edgesColors = [
  lightBlue,
  lightGreen,
  purple,
  orange,
  pink
];

function generateElements(data) {
  return data.nodes.length > 0 && data.relationships.length > 0
    ? data.nodes.map(({ id, labels, properties }) => ({
      data: {
        id: `${id}`,
        labels,
        // parent: `${data.filesRelationship.find(fR => fR.start === id).end}`,
        ...properties
      },
      classes: [properties.type]
    }))
      .concat(data.files.map(({ id, labels, properties }) => ({
        data: {
          id: `${id}`,
          labels,
          name: properties.directory,
          ...properties
        },
        selectable: false,
        // locked: true,
        // grabbable: false,
        classes: [properties.type]
      })))
      .concat(data.relationships.map(({ start, end, type }) => ({
        data: {
          id: `${start}-${end}`,
          source: `${start}`,
          target: `${end}`,
          type: type
        },
        classes: [type]
      })))
      .concat(data.filesRelationship.map(({ start, end, type }) => ({
        data: {
          id: `${start}-${end}`,
          source: `${start}`,
          target: `${end}`,
          type: type
        },
        classes: [type]
      })))
    : [];
}

const AppGraph = ({ selectedNodeId, onSelectNodeId, destinationNodeId, onDestinationNodeId, }) => {
  const classes = useStyles();
  const { loading, data } = DataContext();

  const graphRef = useRef(null);
  const graphInstance = useRef(null);

  const [selected, setSelected] = useState(null);
  const [destination, setDestination] = useState(null);
  const prevDestination = usePrevious(destination);
  const [isGraphAvailable, setGraphAvailable] = useState(false);
  const [viewUtilityApi, setViewUtilityApi] = useState(null);
  const [contextMenuApi, setContextMenuApi] = useState(null);

  useEffect(() => {
    if (data && data.nodes.length > 0 && data.relationships.length > 0 && graphRef.current) {
      setGraphAvailable(true);
      const elements = generateElements(data);
      // initiate graph
      graphInstance.current = cytoscape({
        container: document.getElementById('graph'),
        elements: elements,
        style: [ // the stylesheet for the graph
          ...data.entityTypes.map((entityType, entityTypeIdx) => {
            return {
              selector: `node.${entityType}`,
              style: {
                'background-color': `${entityColors[entityTypeIdx][entityType === 'FILE' ? 100 : 400]}`,
                'label': `data(name)`
              }
            }
          }),
          ...data.relationshipTypes.map((rL, rLIdx) => {
            return {
              selector: `edge.${rL}`,
              style: {
                'width': 2,
                'line-color': `${edgesColors[rLIdx][200]}`,
                'target-arrow-color': `${edgesColors[rLIdx][200]}`,
                'target-arrow-shape': 'triangle',
                'curve-style': 'bezier',
                // 'label': 'data(type)'
              }
            }
          }),
          // {
          //   selector: `edge`,
          //   style: {
          //     'width': 3,
          //     // 'line-color': `${entityColors[9][400]}`,
          //     // 'target-arrow-color': `${entityColors[9][400]}`,
          //     'target-arrow-shape': 'triangle',
          //     'curve-style': 'bezier',
          //     'label': 'data(type)'
          //   }
          // },
          {
            selector: 'highlight',
            style: {
              zIndex: 999,
              'z-index': 999
            }
          },
          {
            selector: 'node.not-highlight, edge.not-highlight',
            style: {
              opacity: .2,
              "z-index": -1
            }
          },
          {
            selector: 'node.rootNode, node.rootNode:selected',
            style: {
              'background-color': '#1458c7'
            }
          },
          {
            selector: 'node.destination',
            style: {
              'background-color': '#7003ff'
            }
          }
        ],
        layout: {
          name: 'fcose', // cose /// fcose  /// cose-bilkent /// avsdf
          animate: false,
          nodeDimensionsIncludeLabels: true
        }
      });

      // viewUtilities
      const viewUtilitiesOptions = {
        setVisibilityOnHide: false, // whether to set visibility on hide/show
        setDisplayOnHide: true, // whether to set display on hide/show
        zoomAnimationDuration: 500, // default duration for zoom animation speed
        neighbor: function (ele) {
          return ele.closedNeighborhood();
        },
        neighborSelectTime: 500,
        lassoStyle: { lineColor: "#d67614", lineWidth: 3 }, // default lasso line color, dark orange, and default line width
        htmlElem4marqueeZoom: '', // should be string like `#cy` or `.cy`. `#cy` means get element with the ID 'cy'. `.cy` means the element with class 'cy'
        marqueeZoomCursor: 'se-resize', // the cursor that should be used when marquee zoom is enabled. It can also be an image if a URL to an image is given
        isShowEdgesBetweenVisibleNodes: true // When showing elements, show edges if both source and target nodes become visible
      };
      setViewUtilityApi(graphInstance.current.viewUtilities(viewUtilitiesOptions));

      // context menu
      var contextMenuOptions = {
        // Customize event to bring up the context menu
        // Possible options https://js.cytoscape.org/#events/user-input-device-events
        evtType: 'cxttap',
        // List of initial menu items
        // A menu item must have either onClickFunction or submenu or both
        menuItems: [
          {
            id: 'set-destination',
            content: 'Set as destination',
            selector: 'node',
            onClickFunction: (event) => {
              if (onDestinationNodeId) {
                setDestination(event.target);
                onDestinationNodeId(event.target.id());
              }
            },
            coreAsWell: true
          },

        ],
        menuItemClasses: [],
        contextMenuClasses: [],
      };
      setContextMenuApi(graphInstance.current.contextMenus(contextMenuOptions));

      // click event
      graphInstance.current.nodes(data.entityTypes.map(label => `.${label}`).join(', ')) // .filter(lb => lb !== 'FILE')
        .on('click', e => {
          e.stopPropagation();
          setSelected(e.target);
          if (onSelectNodeId) onSelectNodeId(e.target.id());
          setDestination(null);
          if (onDestinationNodeId) onDestinationNodeId(null);
        })

      graphInstance.current.on('click', e => {
        e.stopPropagation();
        setSelected(null);
        if (onSelectNodeId) onSelectNodeId(null);
        if (onDestinationNodeId) onDestinationNodeId(null);
      });

      // store the graph instance to windows
      window.cytoGraphInstance = graphInstance.current;
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [data])

  useEffect(() => {
    if (selectedNodeId) {
      if (!selected || `${selectedNodeId}` !== selected.id()) {
        const _selectedNode = graphInstance.current.nodes(`#${selectedNodeId}`);
        if (selected) {
          selected.unselect();
          selected.removeClass('rootNode');
        };
        _selectedNode.select();
        _selectedNode.addClass('rootNode');
        setSelected(_selectedNode);
      }

    } else {
      setSelected(null);
    }

    if (destinationNodeId) {
      if (!destination || `${destinationNodeId}` !== destination.id()) {
        const _destinationNode = graphInstance.current.nodes(`#${destinationNodeId}`);
        setDestination(_destinationNode);
      }

    } else {
      setDestination(null);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [selectedNodeId, destinationNodeId])

  const findPath = (startId, endId, callback) => {
    get(shortestPaths, {
      start: startId,
      end: endId
    }).then(response => {
      if (callback) callback(response);
    })

  }

  useEffect(() => {
    if (graphInstance.current) {
      graphInstance.current.elements('node, edge').removeClass('highlight');
      graphInstance.current.elements('node, edge').removeClass('not-highlight');
      if (selected) {
        selected.addClass('highlight');
      }


      if (selected && destination) {
        findPath(selected.id(), destination.id(), ({ relationships }) => {
          if (relationships && relationships.length > 0) {
            const edgesOfPaths = relationships.map(r => {
              return graphInstance.current.edges(`[id = "${r.start}-${r.end}"]`);
            });


            edgesOfPaths.forEach(edge => {
              edge.addClass('highlight');
              const nodesOfEdge = edge.connectedNodes();
              nodesOfEdge.addClass('highlight');
              // FIXME
              // nodesOfEdge.parent().addClass('highlight');

            })

            if (graphInstance.current.elements('.highlight').length > 0) {
              graphInstance.current.elements().not('.highlight').addClass('not-highlight');
            }

            const neighbours = graphInstance.current.elements('node.highlight');

            if (viewUtilityApi) {
              viewUtilityApi.zoomToSelected(neighbours);
            }

            // setPaths(relationships);
          } else {
            selected.addClass('highlight');
            destination.addClass('highlight');
            graphInstance.current.elements().not('.highlight').addClass('not-highlight');

            if (viewUtilityApi) {
              viewUtilityApi.zoomToSelected(graphInstance.current.elements(`#${selected.id()}, #${destination.id()}`));
            }
          }
        })
      } else if (selected && !destination) {
        // selected.parent().addClass('highlight');
        const neighborhood = selected.neighborhood();
        neighborhood.addClass('highlight');

        // const parents = neighborhood.parent();
        // parents.addClass('highlight');

        if (graphInstance.current.elements('.highlight').length > 0) {
          graphInstance.current.elements().not('.highlight').addClass('not-highlight');
        }

        const neighbours = graphInstance.current.elements('node.highlight');

        if (viewUtilityApi) {
          viewUtilityApi.zoomToSelected(neighbours);
        }
      }


    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [selected, destination, viewUtilityApi, graphInstance])

  useEffect(() => {
    if (prevDestination) prevDestination.removeClass('destination');
    if (destination) destination.addClass('destination');
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [destination]);

  //bind events that have dependencies
  useEffect(() => {
    if (graphInstance.current) {
      graphInstance.current.nodes().not('.FILE').on('cxttap', event => {
        if (selected.id() === event.target.id()) {
          contextMenuApi.disableMenuItem('set-destination');
        } else {
          contextMenuApi.enableMenuItem('set-destination');
        }
      })

      return () => {
        graphInstance.current.nodes().not('.FILE').removeListener('cxttap');
      }
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [selected, graphInstance])

  return (
    <>
      <div id="graph" className={classes.graph} style={{
        display: !loading && isGraphAvailable ? 'flex' : 'none'
      }} ref={graphRef}></div>

      {
        !isGraphAvailable && (
          <div className={classes.noGraph}>
            <p>Graph cannot be loaded</p>
          </div>
        )
      }

      {
        (!loading && !isGraphAvailable) && (<div className={classes.noGraph}>
          <p>graph is loading</p>
        </div>)
      }
    </>
  )
}

export default AppGraph;
