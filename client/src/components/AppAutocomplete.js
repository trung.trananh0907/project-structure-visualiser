import React from 'react';
import { useEffect, useState } from 'react';

import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import CircularProgress from '@material-ui/core/CircularProgress';
import { Grid, Typography } from '@material-ui/core';
import parse from 'autosuggest-highlight/parse';
import { matchSorter } from 'match-sorter';

const AppAutocomplete = ({ id, selectedValue, getOptionsPromise, initOptions, inputComponentFunc, onChange, ...rest }) => {
  const [open, setOpen] = useState(false);
  const [value, setValue] = useState(null);
  const [inputValue, setInputValue] = useState('');
  const [options, setOptions] = useState(initOptions ? initOptions : []);
  const [loading, setLoading] = useState(false);
  useEffect(() => {
    if (selectedValue && (!value || selectedValue !== value.id)) {
      setValue((initOptions || options).find(op => op.id === selectedValue));
    }else if(!selectedValue) {
      setValue(null);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [selectedValue])

  useEffect(() => {
    if (getOptionsPromise) {

      if (inputValue === '') {
        return undefined;
      } else {
        setLoading(true);
      }

      getOptionsPromise(inputValue).then(data => {
        setLoading(false);
        setOptions(data);
      })
    } else {
      return undefined;
    }
  }, [getOptionsPromise, inputValue])

  useEffect(() => {
    if (onChange) {
      onChange(value);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [value]);

  const filterOptions = (options, { inputValue }) => matchSorter(options, inputValue, {
    keys: [
      'properties.name',
      'properties.file'
    ]
  })

  return (
    <Autocomplete
      id={`autocomplete-${id}`}
      style={{ width: 300, marginRight: 10, background: '#fff' }}
      open={open}
      onOpen={() => { setOpen(true) }}
      onClose={() => { setOpen(false) }}
      getOptionSelected={(option, value) => option.name === value.name}
      getOptionLabel={(option) => `${option.labels.includes('FILE') ? option.properties.directory : `${option.properties.name} - ${option.properties.file}`}`}
      options={getOptionsPromise ? options : initOptions}
      filterOptions={filterOptions}
      value={value}
      loading={loading}
      onChange={(event, newValue) => {
        setValue(newValue);

      }}
      {...rest}
      onInputChange={(event, inputValue) => {
        setInputValue(inputValue)
      }}
      renderInput={(params) => inputComponentFunc ? inputComponentFunc(params) : (
        <TextField
          {...params}
          InputProps={{
            ...params.InputProps,
            endAdornment: (
              <>
                {loading ? <CircularProgress color="inherit" size={20} /> : null}
                {params.InputProps.endAdornment}
              </>
            ),
          }}
        />
      )}
      renderOption={(option) => {
        const name = option.properties.name;
        const matchIdxInName = name.toLowerCase().indexOf(inputValue);
        const nameParts = parse(
          name,
          matchIdxInName < 0 ? [] : [[matchIdxInName, matchIdxInName + inputValue.length]]
        );

        const file = option.properties.file;
        const matchIdxInFile = file.toLowerCase().indexOf(inputValue);
        const fileParts = parse(
          file,
          matchIdxInFile < 0 ? [] : [[matchIdxInFile, matchIdxInFile + inputValue.length]]
        );

        return (
          <Grid>
            {nameParts.map((part, index) => (
              <span key={index} style={{ fontWeight: part.highlight ? 700 : 400 }}>
                {part.text}
              </span>
            ))}

            <Typography variant="body2" color="textSecondary">
              {fileParts.map((part, index) => (
                <span key={`fnt-${index}`} style={{ fontWeight: part.highlight ? 700 : 400 }}>
                  {part.text}
                </span>
              ))}
            </Typography>
          </Grid>
        );
      }}
    />
  )
}

export default AppAutocomplete;
