import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import AppAutocomplete from './AppAutocomplete';
import InputBase from '@material-ui/core/InputBase';
import classnames from 'classnames';

const useStyles = makeStyles((theme) => ({
  root: {
    padding: '2px 4px',
    display: 'flex',
    alignItems: 'center',
    width: 500,
  },
  input: {
    marginLeft: theme.spacing(1),
    flex: 1,
  },
  iconButton: {
    padding: 10,
  },
  divider: {
    height: 28,
    margin: 4,
  },
}));

export default function CustomizedSeachBase({ id, selectedValue, label, getOptionsPromise, initOptions, onChange, endAdornment, appInputProps, ...rest }) {
  const classes = useStyles();
  return (
    <AppAutocomplete
      id={id}
      selectedValue={selectedValue}
      getOptionsPromise={getOptionsPromise}
      initOptions={initOptions}
      onChange={value => {
        if (onChange) { onChange(value) }
      }}
      {...rest}
      inputComponentFunc={params => (
        <Paper component="form" className={classes.root} ref={params.InputProps.ref}>
          <InputBase
            className={classnames(classes.input, params.InputProps.className)}
            inputProps={{ 'aria-label': label, ...params.inputProps }}
            endAdornment={params.InputProps.endAdornment}
            {...appInputProps}
          />
          {endAdornment ? endAdornment : (<></>)}
        </Paper>
      )}
    />
  )
}
