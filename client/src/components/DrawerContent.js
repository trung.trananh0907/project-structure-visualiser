import { Typography, makeStyles, Button, List, ListItem, ListItemAvatar, Avatar, ListItemText, Divider, Link } from '@material-ui/core';
import { DataContext } from '../App';
import BookmarkIcon from '@material-ui/icons/Bookmark';
import classNames from 'classnames';
import { useEffect, useState } from 'react';
import { entityColors, edgesColors } from './AppGraph';

const useStyles = makeStyles(theme => ({
  root: {
    margin: '10px',
  },
  infoWrapper: {
    display: 'flex',
    '&>div': {
      width: '50%'
    }
  },
  statusContent: {
    flexGrow: 1
  },
  activity: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start'
  },
  activityContent: {
    marginLeft: 3,
    textTransform: 'none',
    fontWeight: 400,
    fontSize: 12,
    flexGrow: 1,
    justifyContent: 'flex-start'
  },
  activityRoot: {
    display: 'inline-block',
    padding: '0px 3px',
    borderRadius: 4,
    backgroundColor: '#1458c7',
    color: '#fff',
    fontWeight: 500
  },
  activityDestination: {
    backgroundColor: '#7003ff',
    color: '#fff',
    padding: '0px 3px',
    borderRadius: 4,
    fontWeight: 500
  },
  bookmarkIcon: {
    opacity: .5,
    cursor: 'pointer',
    transition: 'opacity 300ms ease-in-out',
    '&:hover': {
      opacity: 1
    },
    '&.bookmarked': {
      opacity: 1
    }
  },
  infoRoot: {
    fontSize: '0.625rem'
  },
  avatarRoot: {
    minWidth: theme.spacing(4)
  },
  avatar: {
    width: 20,
    height: 20
  }
}));

const activityKey = 'graphActivities';

const Activity = ({ rootNode, destinationNode, isBookMarked, onBookmark, onSelect, ...rest }) => {
  const classes = useStyles();

  return (
    <div className={classes.activity} {...rest}>
      <BookmarkIcon className={classNames(classes.bookmarkIcon, {
        'bookmarked': isBookMarked
      })} onClick={() => { if (onBookmark) onBookmark(!isBookMarked); }} />
      <Button className={classes.activityContent} onClick={() => { if (onSelect) onSelect(); }}>
        {!destinationNode
          ? (<>Neighboors of&nbsp;<span className={classes.activityRoot}> {rootNode.properties.name}</span></>)
          : (<>Paths between&nbsp;<span className={classes.activityRoot}> {rootNode.properties.name}</span>&nbsp;and&nbsp;<span className={classes.activityDestination}> {destinationNode.properties.name}</span></>)}
      </Button>
    </div>
  )
}

const DrawerContent = ({ startNodeId, destinationNodeId, onSelectActivity }) => {
  const classes = useStyles();
  const { data } = DataContext();
  const [persistActivities, setPersistActivities] = useState(JSON.parse(localStorage.getItem(activityKey)) || []);
  // const [selectedNodeData, setSelectedNodeData] = useState(null);
  useEffect(() => {
    localStorage.setItem(activityKey, JSON.stringify(persistActivities));
  }, [persistActivities])

  const getNodes = nodeId => {
    if (data && data.nodes.length > 0) {
      return data.nodes.find(n => n.id === +nodeId);
    } else {
      return null;
    }
  }

  const onBookmark = (isBookMark, persistIdx, rootId, destinationId) => {
    if (isBookMark) {
      const persistActivity = !destinationId ? {
        type: 'neighbour',
        rootId
      } : {
        type: 'paths',
        rootId,
        destinationId
      }
      setPersistActivities(persistActivities.concat([persistActivity]));
    } else {
      setPersistActivities(persistActivities.filter((ac, idx) => idx !== persistIdx))
    }
  }

  const renderCurrentProgress = (startNodeId, destinationNodeId) => {
    if (startNodeId && destinationNodeId) {
      const startNode = getNodes(startNodeId);
      const endNode = getNodes(destinationNodeId);
      if (startNode && endNode) {
        return (
          <Activity
            rootNode={startNode}
            destinationNode={endNode}
            isBookMarked={false}
            onBookmark={isBookMarked => { onBookmark(isBookMarked, null, startNodeId, destinationNodeId) }}
            onSelect={() => { if (onSelectActivity) onSelectActivity(startNodeId, destinationNodeId); }}
          />)
      } else {
        return (<></>)
      }
    } else if (startNodeId && !destinationNodeId) {
      const startNode = getNodes(startNodeId);
      if (startNode) {
        return (<Activity
          rootNode={startNode}
          isBookMarked={false}
          onBookmark={isBookMarked => { onBookmark(isBookMarked, null, startNodeId) }}
          onSelect={() => { if (onSelectActivity) onSelectActivity(startNodeId, null); }}
        />)
      } else {
        return (<></>)
      }
    } else if (!startNodeId) {
      return (<></>)
    }
  }

  // useEffect(() => {
  //   if (startNodeId) {
  //     setSelectedNodeData(data.nodes.find(node => node.id === startNodeId));
  //   } else {
  //     setSelectedNodeData(null);
  //   }

  // // eslint-disable-next-line react-hooks/exhaustive-deps
  // }, [startNodeId])

  return (
    <>
      <div className={classNames(classes.root, classes.statusContent)}>
        <Typography variant="h6">Current State</Typography>

        {!startNodeId && !destinationNodeId
          ? (<Typography variant="caption">No state is being represented</Typography>) : renderCurrentProgress(startNodeId, destinationNodeId)}
        <Typography variant="h6">Pinned States</Typography>
        {persistActivities.length === 0
          ? (<Typography variant="caption">No state is pinned</Typography>)
          : persistActivities.map((activity, idx) => {
            const _node = getNodes(activity.rootId);
            let _destinationNode;
            if (activity.type === 'paths') {
              _destinationNode = getNodes(activity.destinationId);
            }
            return activity.type === 'neighbour' ? (
              <>
                {_node ? (<Activity
                  rootNode={_node}
                  key={`persist-activity-${idx}`}
                  isBookMarked={true}
                  onBookmark={isBookMarked => { onBookmark(isBookMarked, idx, activity.rootId) }}
                  onSelect={() => { if (onSelectActivity) onSelectActivity(activity.rootId, null); }}
                />) : (<></>)}
              </>
            ) : (
              <>
                {_node && _destinationNode ? (<Activity
                  rootNode={_node}
                  key={`persist-activity-${idx}-1`}
                  destinationNode={_destinationNode}
                  isBookMarked={true}
                  onBookmark={isBookMarked => { onBookmark(isBookMarked, idx, activity.rootId, activity.destinationId) }}
                  onSelect={() => { if (onSelectActivity) onSelectActivity(activity.rootId, activity.destinationId); }}
                />) : (<></>)}
              </>
            )
          })}
      </div>
      <Divider />

      <div className={classNames(classes.root)}>
        <Typography variant="h6">Selected element</Typography>
        {startNodeId ? (<pre>{JSON.stringify(data.nodes.concat(data.files).find(node => node.id === startNodeId), null, 2)}</pre>) : (<Typography variant='caption'>No element is selected</Typography>)}
      </div>
      <Divider />
      <div className={classNames(classes.root, classes.infoWrapper)}>
        <div>
          <Typography variant="h6">Element types</Typography>
          <List dense={true}>
            {data.entityTypes.map((label, lId) => (
              <ListItem key={`${label}-${lId}-color-item`}>
                <ListItemAvatar className={classes.avatarRoot}>
                  <Avatar variant="rounded" className={classes.avatar} style={{
                    backgroundColor: entityColors[lId][label === 'FILE' ? 100 : 400]
                  }}>&nbsp;</Avatar>
                </ListItemAvatar>
                <ListItemText primaryTypographyProps={{
                  className: classes.infoRoot
                }}>{label}</ListItemText>
              </ListItem>
            ))}
          </List>
        </div>

        <div>
          {/* <Divider /> */}
          <Typography variant="h6">Relationship types</Typography>
          <List dense>
            {data.relationshipTypes.map((rT, rtId) => (
              <ListItem key={`${rT}-${rtId}-edge-colors`}>
                <ListItemAvatar className={classes.avatarRoot}>
                  <Avatar variant="rounded" className={classes.avatar} style={{
                    backgroundColor: edgesColors[rtId][200]
                  }}>&nbsp;</Avatar>
                </ListItemAvatar>
                <ListItemText primaryTypographyProps={{
                  className: classes.infoRoot
                }}>{rT}</ListItemText>
              </ListItem>
            ))}
          </List>
        </div>
      </div>

      <Divider />
      <div className={classes.root}>
        <div>
          <Link
            href="https://github.com/marmelab/react-admin/tree/master/examples/demo"
            rel="noreferrer"
            target="_blank">Link to target project</Link>
        </div>
        <Link
          href="https://gitlab.com/trung.trananh0907/project-structure-visualiser"
          rel="noreferrer"
          target="_blank">Link to dependencies analysis repository</Link>
      </div>
    </>
  )
}

export default DrawerContent;
