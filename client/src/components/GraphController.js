import React, { useEffect, useState } from 'react';

import { makeStyles } from '@material-ui/core/styles';
import CustomizedSeachBase from './SearchInput';
import { DataContext } from '../App';

const useStyles = makeStyles((theme) => ({
  controllers: {
    display: 'flex',
    flexDirection: 'column',
    top: 10,
    left: 10,
    position: 'fixed',
    zIndex: 1000
  },
  controllersControl: {
    display: 'flex',
    margin: '5px 0px',
  },
  root: {
    padding: '2px 4px',
    display: 'flex',
    alignItems: 'center',
    width: 400,
  },
  input: {
    marginLeft: theme.spacing(1),
    flex: 1,
  },
  iconButton: {
    padding: 10,
  },
  divider: {
    height: 28,
    margin: 4,
  },
}));

const GraphController = ({ selectedNodeId, onSelectNodeId, selectedDestinationId, onSelectDestinationId }) => {
  const { data } = DataContext();
  const classes = useStyles();
  const [showFindPath, setShowFindPath] = useState(false);

  useEffect(() => {
    if (!showFindPath && selectedDestinationId) {
      setShowFindPath(true);
    }
  }, [selectedDestinationId, showFindPath])

  return (
    <div className={classes.controllers}>
      <div className={classes.controllersControl}>
        <CustomizedSeachBase
          id="search-node-ac"
          label="Search for an element..."
          selectedValue={selectedNodeId}
          autoHighlight
          onChange={node => {
            if (onSelectNodeId) {
              onSelectNodeId(node ? node.id : null);
            }
          }}
          appInputProps={{
            placeholder: "Search for an element..."
          }}
          initOptions={data ? data.nodes.concat(data.files) : []}
          // endAdornment={(
          //   <>
          //     <Tooltip title="Find paths between dependencies">
          //       <IconButton onClick={() => setShowFindPath(!showFindPath)} color="primary" className={classes.iconButton} aria-label="find-path">
          //         <ShareIcon />
          //       </IconButton>
          //     </Tooltip>
          //   </>
          // )}
        />
      </div>

      {/* style={{
        visibility: showFindPath ? 'visible' : 'hidden'
      }} */}
      <div className={classes.controllersControl}>
        <CustomizedSeachBase
          id="search-destimation-node-ac"
          label="Search for destination..."
          selectedValue={selectedDestinationId}
          disabled={!selectedNodeId}
          autoHighlight
          initOptions={data ? data.nodes.concat(data.files) : []}
          onChange={node => {
            if (onSelectDestinationId) {
              onSelectDestinationId(node ? node.id : null);
            }
          }}
          appInputProps={{
            placeholder: "Search for destination..."
          }}
          // endAdornment={(
          //   <>
          //     <Tooltip title="Find path">
          //       <IconButton onClick={() => { }} type="submit" className={classes.iconButton} aria-label="search">
          //         <NearMeIcon />
          //       </IconButton>
          //     </Tooltip>
          //   </>
          // )}
        />
      </div>
    </div>
  )
}

export default GraphController;
