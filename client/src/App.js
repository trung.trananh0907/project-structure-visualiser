import './App.css';
import { useEffect, useState, createContext, useContext } from 'react';
import { all, get } from './crud';
import AppGraph from './components/AppGraph';
import GraphController from './components/GraphController';
import Drawer from '@material-ui/core/Drawer';
import { makeStyles } from '@material-ui/core/styles';
import DrawerContent from './components/DrawerContent';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    display: 'flex'
  },
  wrapper: {
    height: '100vh',
    flexGrow: 1,
    position: 'relative'
  },
  drawer: {
    width: 500
  },
  drawerPaper: {
    width: 500,
  },
}))

const Context = createContext({});
export const DataContext = init => useContext(Context);

export const DataProvider = ({ children }) => {
  const [data, setData] = useState({
    nodes: [],
    relationships: [],
    files: [],
    filesRelationship: [],
    entityTypes: [],
    relationshipTypes: []
  });

  const [loading, setLoading] = useState(false);

  const getData = () => get(all).then(response => {
    response.nodes = response.nodes.map(node => {
      if (node.properties.appId) {
        delete node.properties.appId;
      }
      return {
        ...node
      }
    })
    setLoading(true);
    setData(response);
    setLoading(false)
  })

  useEffect(() => {
    getData();
  }, [])

  return (
    <Context.Provider value={{ loading, data }}>
      {children}
    </Context.Provider>
  )
}

function App() {
  const classes = useStyles();
  // const [showFindPath, setShowFindPath] = useState(false);
  const [selectNodeId, setSelectNodeId] = useState(null);
  const [destinationNodeId, setDestinationNodeId] = useState(null);

  return (
    <DataProvider>
      <div className={classes.root}>
        <div className={classes.wrapper}>
          <GraphController
            selectedNodeId={selectNodeId}
            onSelectNodeId={nodeId => { setSelectNodeId(nodeId); }}
            selectedDestinationId={destinationNodeId}
            onSelectDestinationId={nodeId => { setDestinationNodeId(nodeId); }}
          />
          <AppGraph
            selectedNodeId={selectNodeId}
            onSelectNodeId={nodeId => setSelectNodeId(+nodeId)}
            destinationNodeId={destinationNodeId}
            onDestinationNodeId={nodeId => setDestinationNodeId(+nodeId)}
          />
        </div>
        <Drawer
          className={classes.drawer}
          variant="permanent"
          anchor="right"
          classes={{
            paper: classes.drawerPaper
          }}
        >
          <DrawerContent
            startNodeId={selectNodeId}
            destinationNodeId={destinationNodeId}
            onSelectActivity={(startNodeId, destinationNodeId) => {
              setSelectNodeId(startNodeId);
              setDestinationNodeId(destinationNodeId);
            }}
          ></DrawerContent>
        </Drawer>
      </div>

    </DataProvider>
  );
}

export default App;
