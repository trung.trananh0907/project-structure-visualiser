import axios from 'axios';

const URI = 'http://localhost:8000';

export const entities = 'entities';
export const relationships = 'relationships';
export const all = 'all';
export const searchNodeURI = 'search-node';
export const shortestPaths = 'shortest-paths';
export const entityTypes = 'entities-types';

export const get = (endpoint, params) => {
  return axios.get(`${URI}/${endpoint}`, {
    params: { ...(params ? params : {}) }
  }).then(response => response.data);
}