1. to reduce the number of 'default' declaration, default only exists if it is an anomous function
2. Q: if it is not possible to identify the source variable => should I keep the import as a node in the graph?
Example:
import ABC from 'some-module';

Explain: ABC is basically a default export from 'some-module' but the name of the export from 'some-module' could be anything

3. define type of declaration: variable, function module, file, interface, classes => need to provide alias type to represent dummy declarations such as 'b' in export {a as b}

refactor:
1. find all imports
2. find all exports
3. find all top level declaration
4. define types of declarations => 
5. RULE => source contains relationship





const a = 1;
export {a};
export {a as b};
// ==============
export const c = 2;
// invalid

// valid 
export const {c: e, d} = 3;
export const [f, g] = [111, 222];

// ======================
Example:
a.js

export {A};
-----------------------------
b.js
-----------------------------
import {A} from 'a.js'

=> A is identical
{
  type: 'import',
  name: A,
  file: 'b.js'
}

import A from b.js should be remembered first time and removed 2nd time as its a declaration from a.js

// ======================
a.js

export default A;

-----------------------------
b.js

import A' from 'a.js'
=> A' is not actual variable name and can be changed to anything => keep 1st and delete 2nd time
=> default A from a.js is declared