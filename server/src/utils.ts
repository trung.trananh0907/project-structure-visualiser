// const path = require('path');
// const fs = require('fs');
// const resolve = require('enhanced-resolve');
import path from 'path';
import fs from 'fs';
import resolve from 'enhanced-resolve';
import { FileBase } from './defines';

// check all files in a folder
export const walkSync = function (dir: string, filelist: FileBase[] = [], fileExtensions?: string[], ignoreFiles?: string[]) {
  let files = fs.readdirSync(dir);
  files.forEach(function (file: string) {
    if (fs.statSync(dir + '/' + file).isDirectory()) {
      if (!ignoreFiles) {
        filelist = module.exports.walkSync(dir + file + '/', filelist, fileExtensions, ignoreFiles);
      } else {
        const fileNotInTheList = ignoreFiles.every((folderName: string) => !`${dir + '/' + file}`.includes(folderName));
        if (fileNotInTheList) {
          filelist = module.exports.walkSync(dir + file + '/', filelist, fileExtensions, ignoreFiles);
        }
      }
    }
    else {
      if (!fileExtensions) {
        if (!ignoreFiles) {
          filelist.push({
            file: dir + file,
            dir: dir,
            fileName: file
          });
        } else {
          const fileNotInTheList = ignoreFiles.every((folderName: string) => !`${dir + '/' + file}`.includes(folderName));
          if (fileNotInTheList) {
            filelist.push({
              file: dir + file,
              dir: dir,
              fileName: file
            });
          }
        }
      } else {
        if (!ignoreFiles) {
          if (fileExtensions.includes(path.extname(file))) {
            filelist.push({
              file: dir + file,
              dir: dir,
              fileName: file
            });
          }
        } else {
          const fileNotInTheList = ignoreFiles.every((folderName: string) => !`${dir + '/' + file}`.includes(folderName));
          if (fileNotInTheList) {
            if (fileExtensions.includes(path.extname(file))) {
              filelist.push({
                file: dir + file,
                dir: dir,
                fileName: file
              });
            }
          }
        }
      }
    }
  });
  return filelist;
}

export const isDirectory = function (path: string) {
  return fs.statSync(path).isDirectory();
}

// resolver for absolute file paths in nodejs application
export const fileResolver = resolve.create.sync({
  // or resolve.create.sync
  extensions: [".ts", ".tsx", ".js", ".jsx"]
  // see more options below
})
