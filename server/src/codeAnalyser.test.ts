import CodeAnalyser from './codeAnalyser';
import { Entity, EntityTypes, FileBase, FileEntity } from './defines';

describe('Test cases', () => {
  const projectFolder = '../test-cases/';
  const fileExtensions = ['.ts'];
  const ignoreFiles = ['file-should-ignore'];

  test('The tool must be able to extract the files structure of the source code', async () => {

    // ACTION
    const codeAnalysis = new CodeAnalyser(projectFolder, fileExtensions, ignoreFiles);
    codeAnalysis.analyse(true);

    let totalCountPromise = Promise.all(codeAnalysis.files.map((file: FileBase) => {
      return codeAnalysis.countLines(file);
    }))

    // ASSERT
    expect(codeAnalysis.files.length).toBe(2);
    expect((await totalCountPromise).reduce((a, b) => a += b)).toBe(33);
  })

  test('The tool must be able to generate AST data from the source code', () => {
    // ACTION
    const codeAnalysis = new CodeAnalyser(projectFolder, fileExtensions, ignoreFiles);
    jest.spyOn(codeAnalysis, 'codeParser');

    codeAnalysis.analyse(true);

    // ASSERT
    expect(codeAnalysis.codeParser).toBeCalledTimes(2);
  })

  test('The tool must be able to generate dependencies information from the source code', () => {
    // ACTION
    const codeAnalysis = new CodeAnalyser(projectFolder, fileExtensions, ignoreFiles);
    jest.spyOn(codeAnalysis, 'analyseFile');
    jest.spyOn(codeAnalysis, 'checkLocalDependencies');

    codeAnalysis.analyse(true);

    // ASSERT
    expect(codeAnalysis.analyseFile).toBeCalledTimes(2);
    expect(codeAnalysis.checkLocalDependencies).toBeCalledTimes(2);
  })


})


