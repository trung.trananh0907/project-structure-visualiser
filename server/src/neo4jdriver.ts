import neo4j from 'neo4j-driver';
import { Driver } from 'neo4j-driver-core';
import { Query } from 'neo4j-driver-core/types/types';


class Neo4jDriver {
  public driver: Driver;
  private databaseName: string;
  constructor(_databaseName: string) {
    this.databaseName = _databaseName;
    this.driver = neo4j.driver(
      'bolt://localhost:7687',
      neo4j.auth.basic('neo4j', '123')
    );
  }

  async query(query: Query, param?: any) {
    const session = this.driver.session({
      database: this.databaseName,
      defaultAccessMode: neo4j.session.WRITE
    });

    try {
      const tx = session.beginTransaction();
      const runQuery = await tx.run(query, { ...(param ? param : {}) });
      const respond = runQuery.records;
      return respond;

    } catch (err) {
      console.error(err);
    }
    finally {
      session.close();
    }
  }

  endDriver() {
    this.driver.close();
  }
}

export default Neo4jDriver;
