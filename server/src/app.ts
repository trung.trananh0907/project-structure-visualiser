import express from 'express';
import Neo4jDriver from './neo4jdriver';
import neo4j, { Record } from 'neo4j-driver';
import CodeAnalyser from './codeAnalyser';
import path from 'path';
import cors from 'cors';
import { from, map, zip } from 'rxjs';
import { Entity, EntityTypes, FileBase, FileEntity } from './defines';

const databaseName: string = 'dependencies';

const projectFolder = '../React-Admin-Demo/';
const fileExtensions = ['.js', '.jsx', '.ts', '.tsx'];
const ignoreFiles = ['node_modules', '.circleci', '.github', '.eslintrc.js', '.git', '.yarn', '.babelrc.js', 'build'];
const codeAnalysis = new CodeAnalyser(projectFolder, fileExtensions, ignoreFiles);

export async function analyse(anaylse: boolean, saveToDb: boolean) {
  // Step 1: analysing code
  codeAnalysis.analyse(anaylse);
  
  const totalCounrtPromise = Promise.all(codeAnalysis.files.map((file: FileBase) => {
    return codeAnalysis.countLines(file);
  }))

  let totalCount = (await totalCounrtPromise).reduce((a, b) => a += b);
  
  if (saveToDb) {
    // Step 2: add data into database
    let graphDriver = new Neo4jDriver(databaseName);

    const session = graphDriver.driver.session({
      database: databaseName,
      defaultAccessMode: neo4j.session.WRITE
    })
    const tx = session.beginTransaction();
    try {
      const getStatusRecord = await tx.run('MATCH (n:STATUS) RETURN n.status as status');
      const record = getStatusRecord.records[0];
      if (record) {
        const status = record.get('status');
        if (status) {
          console.log('already analysed => clear db before storing new data');
          await tx.run(`MATCH (n) DETACH DELETE n`);
        }
      }

      // add file entity
      codeAnalysis.entities.forEach(async (entity: Entity | FileEntity) => {

        if (entity.type != EntityTypes.File) {
          const _entity = entity as Entity;

          await tx.run(`CREATE (:ELEMENT {appId: $id, type: $type, name: $name, start: $start, end: $end})`, {
            ..._entity
          })
        } else if (entity.type == 'File') {
          const _entity = entity as FileEntity;

          await tx.run(`CREATE (:${_entity.type.toUpperCase()} {appId: $id, type: $type, fileName: $fileName, directory: $directory})`, {
            id: _entity.id,
            fileName: _entity.fileName,
            directory: _entity.file,
            type: entity.type
          });
        }
      })
      
      // add relationships
      codeAnalysis.entities.forEach(async (entity: Entity | FileEntity) => {
        if (entity.relationships.length > 0) {
          entity.relationships.forEach(async ({ target, type }) => {

            await tx.run(`
            MATCH
              (s {appId: $sourceId}),
              (t {appId: $targetId})
            CREATE (s)-[r:${type}]->(t)
            RETURN s, r, t;
            `, {
              sourceId: entity.id,
              targetId: target
            });
          })
        }

      })

      await tx.run('CREATE (st:STATUS {status: $status})', {
        status: true
      });

      await tx.commit();
    } catch (ex) {
      console.log(ex);
      tx.rollback();
    } finally {
      session.close();
    }

    graphDriver.endDriver();
  }

  console.log(`Total analysed lines of code: ${totalCount}`);
  console.log(`Total analysed files: ${codeAnalysis.files.length}`);
}

analyse(true, true);

const app = express();
const port = 8000;

app.use(cors());

// APIs
// ====================================================================
// eslint-disable-next-line no-undef
app.use('/', express.static(path.join(__dirname, '../client/build')))

app.get('/entities', function (req: any, res: any) {
  res.send(codeAnalysis.entities);
})

app.get('/entities-types', (req: any, res: any) => {
  const graphDriver = new Neo4jDriver(databaseName);

  graphDriver.query(`
  MATCH(n)
  WHERE all(label IN labels(n) WHERE label <> 'STATUS'
    AND label <> 'FILE')
  RETURN DISTINCT labels(n)
  `).then((labels: any[] | undefined) => labels ? labels.map(label => {
    console.log(label);
    return label;
  }) : []);

})

// TODO
app.get('/search-node', (req: any, res: any) => {
  const query = req.query;
  const keyword = query['keyword'];

  const graphDriver = new Neo4jDriver(databaseName);

  graphDriver.query(`
    MATCH (n)
      WHERE n.name CONTAINS $keyword
    RETURN n
  `, {
    keyword
  })
    .then((nodes: Record[] | undefined) => nodes ? nodes.map(node => {
      const _n = node.get('n');
      if (_n) {
        return {
          id: _n.identity.toNumber(),
          labels: _n.labels,
          properties: { ..._n.properties }
        }
      } else {
        return null;
      }
    }) : [])
    .then((nodes: any[]) => {
      res.send(nodes);
    })

  graphDriver.endDriver();
})

app.get('/all', (req: any, res: any) => {

  const graphDriver = new Neo4jDriver(databaseName);

  const nodesRx = from(graphDriver.query(`
    MATCH (n)-->(f:FILE)
    RETURN n, f.directory
  `)).pipe(
    map((nodesData: Record[] | undefined) => nodesData ? nodesData.map(node => {
      const _n = node.get('n');
      const _directory = node.get('f.directory');

      return {
        id: _n.identity.toNumber(),
        labels: _n.labels,
        properties: {
          ..._n.properties,
          file: _directory
        }
      }
    }).filter(node => !node.labels.includes('FILE') && !node.labels.includes('STATUS')) : [])
  );

  const relationshipsRx = from(graphDriver.query(`
    match p = ()-[:AFFECT | :EXPORTED_BY | :REPRESENTED_BY]->()
    return relationships(p)
  `)).pipe(
    map((relationshipsData: Record[] | undefined) => relationshipsData ? relationshipsData.map(relationship => {
      const _r = relationship.get('relationships(p)')[0];
      if (_r) {
        return {
          start: _r.start.toNumber(),
          end: _r.end.toNumber(),
          type: _r.type
        }
      } else {
        return null;
      }
    }) : [])
  );

  const filesRx = from(graphDriver.query(`
    MATCH (f:FILE)
    RETURN f
  `)).pipe(
    map((filesData: Record[] | undefined) => filesData ? filesData.map(fileData => {
      const _f = fileData.get('f');
      return {
        id: _f.identity.toNumber(),
        labels: _f.labels,
        properties: { ..._f.properties }
      }
    }) : [])
  );

  const filesRelationshipRx = from(graphDriver.query(`
    MATCH (f:FILE)
    OPTIONAL MATCH (f)<-[r:PART_OF]-(n)
    RETURN r
  `)).pipe(
    map((filesRelationshipData: Record[] | undefined) => filesRelationshipData ? filesRelationshipData.map(frData => {
      const _r = frData.get('r');
      if (_r) {
        return {
          start: _r.start.toNumber(),
          end: _r.end.toNumber(),
          type: _r.type
        }
      } else {
        return null;
      }
    }) : [])
  );

  const entityTypesRx = from(graphDriver.query(`
  MATCH (n)
  RETURN DISTINCT PROPERTIES(n).type
  `)).pipe(
    map((labels: Record[] | undefined) => labels ? labels.map(label => {
      const _l = label.get('PROPERTIES(n).type');
      return _l || null;
    }) : [])
  )

  const relationshipTypesRx = from(graphDriver.query(`
    match ()-[r]->()
    return distinct type(r)
  `)).pipe(
    map((relationshipTypes: Record[] | undefined) => relationshipTypes ? relationshipTypes.map(rT => {
      return rT.get('type(r)');
    }) : [])
  );

  zip([
    nodesRx,
    relationshipsRx,
    filesRx,
    filesRelationshipRx,
    entityTypesRx,
    relationshipTypesRx
  ]).subscribe(([nodes, relationships, files, filesRelationship, entityTypes, relationshipTypes]: [any[], any[], any[], any[], any[], any[]]) => {
    res.send({
      nodes: nodes.filter(n => n != null && n != undefined).map(node => ({
        ...node,
        properties: {
          ...node.properties
        }
      })),
      relationships: relationships.filter(r => r != null && r != undefined),
      files: files.filter(f => f != null && f != undefined),
      filesRelationship: filesRelationship.filter(fr => fr != null && fr != undefined),
      entityTypes: entityTypes.filter(lb => lb != null && lb != undefined),
      relationshipTypes: relationshipTypes.filter(rT => rT != null && rT != undefined),
    })
  })

  graphDriver.endDriver();
})

app.get('/shortest-paths', (req: any, res: any) => {
  const query = req.query;
  const startId = query['start'];
  const endId = query['end'];

  const graphDriver = new Neo4jDriver(databaseName);

  graphDriver.query(`
    MATCH p = allShortestPaths((start)-[*]->(end))
      where ID(start) = $start and all(label in labels(start) where label <>'FILE')
      and ID(end) = $end and all(label in labels(end) where label <>'FILE')
    return p
  `, {
    start: +startId,
    end: +endId
  }).then((data: Record[] | undefined) => {
    const paths = data ? data.map(path => {
      const _p = path.get('p');
      return _p;
    }) : [];

    // get nodes from paths
    const allNodes = paths.map(path => {
      const segments = path.segments;
      return segments.map((segment: any) => [segment.start, segment.end]).flat();
    }).flat()
      .filter((node, nodeIdx, arr) => {
        const _nodeIdx = arr.findIndex(_node => _node.identity.toNumber() == node.identity.toNumber());
        return _nodeIdx === nodeIdx;
      })
      .map(({ identity, labels, properties }) => ({
        id: identity.toNumber(),
        labels,
        properties
      }));

    const nodes = allNodes.filter(node => !node.labels.includes('FILE'));
    const files = allNodes.filter(node => node.labels.includes('FILE'));

    const allRelationships = paths.map(path => {
      const segments = path.segments;
      return segments.map((segment: any) => ({
        start: segment.relationship.start.toNumber(),
        end: segment.relationship.end.toNumber(),
        type: segment.relationship.type
      }))
    }).flat();

    const nodeRelationship = allRelationships.filter(relationship => relationship.type != 'PART_OF');
    const fileRelationship = allRelationships.filter(relationship => relationship.type == 'PART_OF');

    res.send({
      nodes: nodes,
      relationships: nodeRelationship,
      files: files,
      filesRelationship: fileRelationship
    });

    graphDriver.endDriver()
  })


})

app.listen(port, function () {
  console.log('listening');
})
