import { Node } from '@babel/types';
export enum EntityTypes {
  File = 'File',
  Alias = 'Alias',
  // an imported module is treated as a local declaration => need tracing relationship from 2nd
  ImportNamespace = 'ImportNamespace',
  Namespace = 'Namespace',

  // should remove later
  ImportNamed = 'ImportNamed',
  // an import default can trace to actual declaration of a source file, a default export can be kept if it is represented by an arrow function => need to do in 2nd
  ImportDefault = 'ImportDefault',

  Variable = 'Variable',
  TSModule = 'TSModule',
  TSTypeAlias = 'TSTypeAlias',
  TSInterface = 'TSInterface',
  Function = 'Function',
  Class = 'Class',
  ExportDefault = 'ExportDefault'
}

export interface EntityBase {
  id: number;
  type: EntityTypes;
  relationships: Relationship[];
  extra?: { [key: string]: any }
}

export interface Entity extends EntityBase {
  name: string; // key
  file: string; // key
  start: number | null;
  end: number | null;
  isExported?: boolean;
  // used for imports
  // true means cannot trace the actual source due to compiling error => keep as reference in 2nd
  // fasle means file is explicit
  // undefined means not applicable
  importImplicit?: boolean;
}

export interface FileBase {
  file: string;
  dir: string;
  fileName: string;
}

export interface FileEntity extends EntityBase, FileBase { }

export type RelationTypes = 'PART_OF' | 'AFFECT' | 'REPRESENTED_BY' | 'EXPORTED_BY';

export interface Relationship {
  target: number;
  type: RelationTypes;
}
