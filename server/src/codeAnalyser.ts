import path from 'path';
import { parse } from '@babel/parser';
import fs from 'fs';
import traverse, { NodePath } from '@babel/traverse';
import * as utils from './utils';
import readline from 'readline';
import {
  Entity,
  EntityTypes,
  FileBase,
  FileEntity,
  Relationship,
  RelationTypes
} from './defines';
import {
  ExportDefaultDeclaration,
  ExportSpecifier,
  FunctionDeclaration,
  Identifier,
  ObjectProperty,
  Program,
  Statement,
  StringLiteral,
  TSInterfaceDeclaration,
  TSTypeAliasDeclaration,
  VariableDeclaration,
  File as BabelFile,
  ImportNamespaceSpecifier,
  ImportSpecifier,
  ImportDefaultSpecifier,
  Node as BabelNode,
  JSXIdentifier,
  identifier,
  ObjectPattern,
  ArrayPattern,
  ArrowFunctionExpression,
  FunctionExpression,
  RestElement,
  PatternLike,
  AssignmentPattern,
  CallExpression,
  VariableDeclarator

} from '@babel/types';

// NOTE helpers

function createEntity(id: number,
  name: string,
  type: EntityTypes,
  file: string,
  start: number | null,
  end: number | null,
  relationships: Relationship[] = [],
  importImplicit?: boolean,
  isExported?: boolean,
  extra?: { [key: string]: any }
): Entity {
  return { id, name, type, file, relationships, importImplicit, start, end, isExported, extra };
}

function checkDuplicateEntity(entity: Entity, name: string, type: EntityTypes): boolean {
  return entity.name == name && entity.type == type;
}

class CodeAnalyser {
  public files: FileBase[];
  public fileExtensions: string[];
  public ignoreFiles: string[];
  public directory: string;

  public dataByFiles: { [fileKey: string]: Entity[] };
  public entities: (Entity | FileEntity)[];

  public counter: number = 0;

  public ast: BabelFile | null | undefined;
  constructor(directory: string, fileExtensions: string[], ignoreFiles: string[]) {
    this.files = [];
    this.fileExtensions = fileExtensions;
    this.ignoreFiles = ignoreFiles;
    this.directory = directory;

    this.dataByFiles = {};
    this.entities = [];
  }

  public findDependency(entity: Entity, currentFilePath: string, rootEntity?: Entity) {
    if (entity.importImplicit == true) return;
    if (entity.file != currentFilePath) {
      // get to the source dependency
      const sourceFileData = this.dataByFiles[entity.file];
      if (sourceFileData == undefined) return;
      if (entity.type == EntityTypes.ImportNamespace) {
        const namespaceEntity = sourceFileData.find(_e => _e.id === entity.relationships[0].target);
        if (namespaceEntity != undefined) {
          sourceFileData.forEach(sourceEntity => {
            if (sourceEntity.isExported || sourceEntity.type == EntityTypes.ExportDefault) {
              // remove all duplicate relationship
              const uniqueNameSpaceRelations = namespaceEntity.relationships.filter(nsRelation => {
                return sourceEntity.relationships.findIndex(seR => seR.target === nsRelation.target && seR.type === nsRelation.type) < 0;
              })

              sourceEntity.relationships = sourceEntity.relationships.concat(uniqueNameSpaceRelations);
            }
          })

        }
      } else {
        const sourceEntity = sourceFileData.find(_sourceEntity => {
          if (entity.type === EntityTypes.ImportDefault) {
            return _sourceEntity.name === 'default' && _sourceEntity.type === EntityTypes.ExportDefault;
          } else if (entity.type === EntityTypes.ImportNamed) {
            return _sourceEntity.name === entity.name;
          }
        });

        if (sourceEntity != undefined) {
          this.findDependency(sourceEntity as Entity, entity.file, rootEntity || entity);
        }
      }

    } else {
      if (rootEntity != undefined) {
        if (entity.extra?.shouldDelete && entity.extra != undefined && entity.extra.sourceId) {
          const sourceFileData = this.dataByFiles[entity.file];
          if (sourceFileData == undefined) return;
          const furtherRootEntity = sourceFileData.find(en => en.id === (entity.extra != undefined && entity.extra.sourceId));
          if (furtherRootEntity != undefined) {
            furtherRootEntity.relationships = furtherRootEntity.relationships.concat(rootEntity.relationships.filter(rEr => {
              return !furtherRootEntity.relationships.find(fRR => fRR.target == rEr.target && fRR.type == rEr.type);
            }))
          }
        } else {
          // check if relation exists => if not add relation
          rootEntity.relationships.forEach(relationship => {
            const existRelation = entity.relationships.find(_r => _r.target === relationship.target && _r.type === relationship.type);
            if (!existRelation) {
              entity.relationships = entity.relationships.concat([{ ...relationship }]);
            }
          })
        }
      }
    }
  }

  checkIdentifierDependency(existedEntity: Entity, nestedPath: NodePath<Identifier | JSXIdentifier>, fileRelationship: Entity[], parentNode?: BabelNode) {
    const nestedIdName = nestedPath.node.name;
    // check if an existing variable
    const sourceEntity = fileRelationship.find(entity => entity.name === nestedIdName);
    if (sourceEntity != undefined && existedEntity.name != sourceEntity.name) { // existedEntity.name != sourceEntity.name

      const type: RelationTypes = existedEntity.type === EntityTypes.ExportDefault && (parentNode as ExportDefaultDeclaration).declaration.type == 'Identifier' ? 'EXPORTED_BY' : 'AFFECT';

      // if (type == 'EXPORTED_BY') {
      //   sourceEntity.relationships = sourceEntity.relationships.concat(existedEntity.relationships.filter(eEr => {
      //     return !sourceEntity.relationships.find(sEr => sEr.target === eEr.target && sEr.type === eEr.type);
      //   }));

      //   existedEntity.extra = {
      //     ...existedEntity.extra,
      //     shouldDelete: true
      //   }
      // } else {
      //   sourceEntity.relationships = sourceEntity.relationships.concat([{
      //     target: existedEntity.id,
      //     type: type
      //   }]);

      //   sourceEntity.isExported = true;
      // }

      sourceEntity.relationships = sourceEntity.relationships.concat([{
        target: existedEntity.id,
        type: type
      }])
      if (type === 'EXPORTED_BY') {
        existedEntity.extra = {
          ...existedEntity.extra,
          sourceId: sourceEntity.id,
          shouldDelete: true
        }
      }
    }
  }

  // NOTE CHECK RELATIONSHIP
  public checkLocalDependencies(fileRelationship: Entity[]) {
    // check relationship from variable declaration => it is necessary since a function can be assigned to a variable
    if (!this.ast) {
      console.error('there is no ast to analyse');
      return;
    }

    traverse(this.ast, {
      // check relationships of variables
      VariableDeclarator: astPath => {
        let existedEntity = fileRelationship.find(entity => entity.name == (astPath.node.id as Identifier).name); // may also check location
        if (existedEntity != undefined) {
          traverse(astPath.node, {
            Identifier: nestedPath => this.checkIdentifierDependency(existedEntity as Entity, nestedPath, fileRelationship),
            JSXIdentifier: nestedPath => this.checkIdentifierDependency(existedEntity as Entity, nestedPath, fileRelationship),
          }, astPath.scope, null, undefined);
        }
      },
      // check relationships for functions => react component can be defined as a function
      FunctionDeclaration: astPath => {
        let existedEntity = fileRelationship.find(entity => entity.name == (astPath.node.id as Identifier).name);

        if (existedEntity) {
          traverse(astPath.node, {
            Identifier: nestedPath => this.checkIdentifierDependency(existedEntity as Entity, nestedPath, fileRelationship),
            JSXIdentifier: nestedPath => this.checkIdentifierDependency(existedEntity as Entity, nestedPath, fileRelationship),
          }, astPath.scope, null, astPath.parentPath);
        }
      },
      TSInterfaceDeclaration: astPath => {
        let existedEntity = fileRelationship.find(entity => entity.name == (astPath.node.id as Identifier).name);

        if (existedEntity) {
          traverse(astPath.node, {
            Identifier: nestedPath => this.checkIdentifierDependency(existedEntity as Entity, nestedPath, fileRelationship),
            JSXIdentifier: nestedPath => this.checkIdentifierDependency(existedEntity as Entity, nestedPath, fileRelationship),
          }, astPath.scope, null, astPath.parentPath);
        }
      },
      // check replationship for classes => react component can be also be defined as a class
      // TODO: 
      // check relationships for exports
      ExportDefaultDeclaration: astPath => {
        let existedEntity = fileRelationship.find(entity => entity.name == 'default');

        if (existedEntity) {
          traverse(astPath.node, {
            Identifier: nestedPath => this.checkIdentifierDependency(existedEntity as Entity, nestedPath, fileRelationship, astPath.node),
            JSXIdentifier: nestedPath => this.checkIdentifierDependency(existedEntity as Entity, nestedPath, fileRelationship, astPath.node),
          }, astPath.scope, null, astPath.parentPath);
        }
      }
    });
  }

  repeatableDeclarationIdentify(root: Statement, astPath: NodePath, fileRelationship: Entity[], filePath: FileBase, isExported?: boolean) {
    switch (root.type) {
      // =====================================================
      // DONE
      case 'VariableDeclaration':
        root.declarations.forEach(_d => {
          if (!(_d.id as Identifier).name) {
            if (_d.id.type == 'ArrayPattern') {
              // example const [a,b] = [1,2];
              traverse(_d.id, {
                Identifier: nestedVariable => {
                  const existedVariableIdx = fileRelationship.findIndex(entity => checkDuplicateEntity(entity, nestedVariable.node.name, EntityTypes.Variable));
                  if (existedVariableIdx == -1) {
                    const id = this.counter;
                    fileRelationship.push(createEntity(id,
                      nestedVariable.node.name,
                      EntityTypes.Variable,
                      filePath.file,
                      nestedVariable.node.start,
                      nestedVariable.node.end,
                      [],
                      undefined,
                      isExported
                    ));
                    this.counter++;
                  }
                }
              }, astPath.scope, astPath.parentPath);
            } else if (_d.id.type == 'ObjectPattern') {
              _d.id.properties.forEach(p => {
                const _p = p as ObjectProperty;
                const existedVariableIdx = fileRelationship.findIndex(entity => checkDuplicateEntity(entity, (_p.value as Identifier).name, EntityTypes.Variable));

                if (existedVariableIdx == -1) {
                  const id = this.counter;

                  fileRelationship.push(createEntity(id,
                    (_p.value as Identifier).name,
                    EntityTypes.Variable,
                    filePath.file,
                    _p.value.start,
                    _p.value.end,
                    [],
                    undefined,
                    isExported,
                  ));
                  this.counter++;
                }
              })
            }

          } else {
            // example const a = 1;
            const existedVariableIdx = fileRelationship.findIndex(entity => checkDuplicateEntity(entity, (_d.id as Identifier).name, EntityTypes.Variable));

            if (existedVariableIdx == -1) {
              const id = this.counter;

              fileRelationship.push(createEntity(id,
                (_d.id as Identifier).name,
                EntityTypes.Variable,
                filePath.file,
                _d.id.start,
                _d.id.end,
                [],
                undefined,
                isExported,
              ));
              this.counter++;
            }
          }
        });
        break;

      // =====================================================
      // DONE
      case 'TSModuleDeclaration':
        // eslint-disable-next-line no-case-declarations
        let moduleName = (root.id as Identifier).name || (root.id as StringLiteral).value;

        const existedModuleDeclarationIdx = fileRelationship.findIndex(entity => checkDuplicateEntity(entity, moduleName, EntityTypes.TSModule));

        if (existedModuleDeclarationIdx == -1) {
          const id = this.counter;

          fileRelationship.push(createEntity(id,
            moduleName as string,
            EntityTypes.TSModule,
            filePath.file,
            root.id.start,
            root.id.end,
            [],
            undefined,
            false,
          ));
          this.counter++;
        }

        break;

      // =====================================================
      // DONE
      case 'ClassDeclaration':
      case 'TSTypeAliasDeclaration':
      case 'TSInterfaceDeclaration':
      case 'FunctionDeclaration':
        const declarationName = (root.id as Identifier).name;
        const declarationType = root.type === 'TSTypeAliasDeclaration' ? EntityTypes.TSTypeAlias
          : root.type === 'TSInterfaceDeclaration' ? EntityTypes.TSInterface
            : root.type === 'FunctionDeclaration' ? EntityTypes.Function : EntityTypes.Class;

        const existedTypeAliasDeclarationIdx = fileRelationship.findIndex(entity => checkDuplicateEntity(entity, declarationName, declarationType));

        if (existedTypeAliasDeclarationIdx == -1) {
          const id = this.counter;
          let _path;
          if (root.type == 'TSTypeAliasDeclaration') {
            _path = root.typeAnnotation;
          } else if (root.type == 'TSInterfaceDeclaration') {
            _path = [root.extends, root.body, root.typeParameters];
          } else if (root.type == 'FunctionDeclaration') {
            _path = [root.params, root.body, root.typeParameters];
          } else if (root.type == 'ClassDeclaration') {
            _path = [root.superClass, root.superTypeParameters, root.body, root.typeParameters];
          }

          fileRelationship.push(createEntity(id,
            declarationName,
            declarationType,
            filePath.file,
            root.start,
            root.end,
            [],
            undefined,
            false,
          ));
          this.counter++;
        }
        break;
    };
  }

  public codeParser(filePath: FileBase) {
    // loop through the list of files and list all imports, exports, declarations
    const content = fs.readFileSync(filePath.file, 'utf8');
    // let ast: BabelFile | null | undefined;
    try {
      this.ast = parse(content, {
        sourceFilename: filePath.file,
        sourceType: 'unambiguous',
        plugins: [
          "asyncDoExpressions",
          "asyncGenerators",
          "bigInt",
          "classPrivateMethods",
          "classPrivateProperties",
          "classProperties",
          "classStaticBlock",
          "decimal",
          "doExpressions",
          "dynamicImport",
          "estree",
          "exportDefaultFrom",
          // "flow",
          "flowComments",
          "functionBind",
          "functionSent",
          "importMeta",
          "typescript",
          "jsx",
          "logicalAssignment",
          "importAssertions",
          "moduleBlocks",
          "moduleStringNames",
          "nullishCoalescingOperator",
          "numericSeparator",
          "objectRestSpread",
          "optionalCatchBinding",
          "optionalChaining",
          "partialApplication",
          "privateIn", // Enabled by default
          "throwExpressions",
          "topLevelAwait",
        ],
        allowImportExportEverywhere: true,
        allowReturnOutsideFunction: true,
        allowAwaitOutsideFunction: true,
        allowSuperOutsideMethod: true,
        allowUndeclaredExports: true,
        // attachComment: true,
        createParenthesizedExpressions: true,
        errorRecovery: true
      });
    } catch (err) {
      console.log(err);
      console.log(`cannot parse file ${filePath.file} due to syntax error`);
    }
  }

  /**
   * 
   * @param {string} filePath path to file need to analyse
   * @returns 
   */
  analyseFile(filePath: FileBase) {
    // file relationship object
    let fileRelationship: Entity[] = [];


    // check the imported components
    // credit: https://gist.github.com/kethinov/6658166

    // if parsing not successful
    if (!this.ast) return;

    traverse(this.ast, {
      Program: (astPath: NodePath<Program>) => {
        const programBody = astPath.node.body;
        programBody.forEach((element: Statement) => {
          switch (element.type) {
            case 'ImportDeclaration':
              // get file's absolute path
              // eslint-disable-next-line no-case-declarations
              let fromModuleAbsolutePath: string | false;
              let canGetActualFilePath: boolean = true;
              try {
                fromModuleAbsolutePath = utils.fileResolver(filePath.dir, element.source.value, function (err: any, result: any) {
                  if (err) {
                    console.log('cannot find source module');
                    return element.source.value;
                  }
                  return result;
                });
              } catch (ex) {
                canGetActualFilePath = false;
                fromModuleAbsolutePath = element.source.value;
              }

              // add import identity to file relationship
              element.specifiers.forEach(specifier => {
                const alias = specifier.local.name;
                let importedName: string;
                let entityId: number;
                switch (specifier.type) {
                  case 'ImportSpecifier':
                    // check if an imported value is being referenced by another name
                    importedName = (specifier.imported as Identifier).name;
                    if (alias !== importedName) {
                      const id = this.counter;
                      fileRelationship.push(createEntity(id, alias, EntityTypes.Alias, filePath.file, specifier.imported.start, specifier.imported.end))
                      this.counter++;
                    }

                    entityId = this.counter;
                    fileRelationship.push({
                      ...createEntity(entityId, importedName, EntityTypes.ImportNamed, fromModuleAbsolutePath as string, specifier.imported.start, specifier.imported.end),
                      importImplicit: !canGetActualFilePath,
                      relationships: alias !== importedName ? [
                        {
                          target: entityId - 1,
                          type: 'REPRESENTED_BY'
                        }
                      ] : []
                    })
                    this.counter++;

                    break;
                  case 'ImportDefaultSpecifier':
                    importedName = specifier.local.name;
                    entityId = this.counter;

                    fileRelationship.push(
                      createEntity(entityId, importedName, /* 'default' */ EntityTypes.ImportDefault, fromModuleAbsolutePath as string, specifier.local.start, specifier.local.end, undefined, !canGetActualFilePath)
                    )
                    this.counter++;

                    break;
                  case 'ImportNamespaceSpecifier':
                    importedName = specifier.local.name;
                    entityId = this.counter;

                    fileRelationship.push(
                      // namespace is declared in the current file so its origin should be in this file
                      createEntity(entityId, importedName, EntityTypes.Namespace, filePath.file, specifier.local.start, specifier.local.end)
                    )
                    this.counter++;

                    //add import namespace to get the dependencies in stage 2
                    entityId = this.counter;
                    // namespace is declared in the current file so its origin should be in this file

                    fileRelationship.push({
                      ...createEntity(entityId, '*', EntityTypes.ImportNamespace, fromModuleAbsolutePath as string, specifier.local.start, specifier.local.end),
                      importImplicit: !canGetActualFilePath,
                      relationships: [
                        {
                          target: entityId - 1,
                          type: 'REPRESENTED_BY'
                        }
                      ]
                    });

                    this.counter++;

                    break;
                }
              });
              break;
            case 'VariableDeclaration':
            case 'TSModuleDeclaration':
            case 'TSTypeAliasDeclaration':
            case 'TSInterfaceDeclaration':
            case 'FunctionDeclaration':
            case 'ClassDeclaration':
              this.repeatableDeclarationIdentify(element, astPath, fileRelationship, filePath);
              break;
            // =====================================================

            case 'ExportNamedDeclaration':
              if (element.specifiers.length > 0) {
                element.specifiers.forEach(specifier => {
                  const _specifier = specifier as ExportSpecifier;
                  const localVariable = fileRelationship.find(entity => entity.name == _specifier.local.name);

                  if (_specifier.local.name != (_specifier.exported as Identifier).name) {
                    const aliasId = this.counter;

                    fileRelationship.push(
                      createEntity(aliasId, (_specifier.exported as Identifier).name, EntityTypes.Alias, filePath.file, _specifier.exported.start, _specifier.exported.end)
                    );
                    this.counter++;
                    if (localVariable != undefined) {
                      if (!localVariable.relationships || localVariable.relationships.length == 0) {
                        localVariable.relationships = [{
                          target: aliasId,
                          type: 'REPRESENTED_BY'
                        }];

                      } else {
                        localVariable.relationships.push({
                          target: aliasId,
                          type: 'REPRESENTED_BY'
                        })
                      }
                    }
                  } else {
                    if (localVariable != undefined) {
                      localVariable.isExported = true;
                    }
                  }
                })
              } else if (element.declaration) {
                // NOTE export named
                this.repeatableDeclarationIdentify(element.declaration, astPath, fileRelationship, filePath, true);
              }
              break;

            case 'ExportDefaultDeclaration':
              const entityIdx = this.counter;
              fileRelationship.push(createEntity(entityIdx,
                'default',
                EntityTypes.ExportDefault,
                filePath.file,
                element.declaration.start,
                element.declaration.end,
                [],
                undefined,
                false
              ));
              this.counter++;
              break;
          }
        })
      }
    })

    this.dataByFiles[filePath.file] = fileRelationship;
  }

  analyseDependencies() {
    // do cross file analyse when all files data is available
    // remove all imports, exports and get to the original declaration


    // remove files which don't include any entity
    const fileNamesHaveEntitiesOnly = Object.keys(this.dataByFiles)
      .filter(fileName => this.dataByFiles[fileName].length > 0);

    const filesListHaveEntitiesOnly = this.files.filter(filePath => fileNamesHaveEntitiesOnly.includes(filePath.file));

    // add file
    this.entities = this.entities.concat(filesListHaveEntitiesOnly.map((file, fileIdx) => ({
      id: this.counter + 1 + fileIdx,
      type: EntityTypes.File,
      relationships: [],
      ...file
    } as FileEntity)));

    Object.keys(this.dataByFiles).forEach(filePath => {
      const fileData = this.dataByFiles[filePath];

      fileData.forEach(entity => {
        // find dependencies for very entity in individual files
        this.findDependency(entity, filePath)

        // find file ID from entities list
        const fileFromEntity = this.entities.find(_e => _e.type == EntityTypes.File && (_e as FileEntity).file == filePath);
        if (fileFromEntity != undefined) {
          // add relationship between Node and File
          entity.relationships = entity.relationships.concat([{
            target: fileFromEntity.id,
            type: 'PART_OF'
          }])
        }
      });
    });


    // flat data from all files
    const inFilesEntities = Object.values(this.dataByFiles)
      .flat()
      // remove all import types
      .filter(entity => ![EntityTypes.ImportNamed, EntityTypes.ImportDefault, EntityTypes.ImportNamespace, EntityTypes.Namespace].includes(entity.type))
      // remove all default exports that just represent other variable
      .filter(entity => !(entity.type == EntityTypes.ExportDefault && entity.extra?.shouldDelete))
    // remove all local variable
    // .filter(entity => entity.isExported);

    // add entities from fileData
    this.entities = this.entities.concat(inFilesEntities);
  }

  public fileStructure() {
    this.files = utils.walkSync(this.directory, undefined, this.fileExtensions, this.ignoreFiles);
  }


  public analyse(analyse: boolean) {
    this.counter = 0;
    // check for file structure
    this.fileStructure();

    // analyse the list of files
    this.files.forEach(((filePath: FileBase) => {
      if (analyse) {
        // return AST from code
        this.codeParser(filePath);

        // identify code elements
        this.analyseFile(filePath);

        // check local dependencies
        this.checkLocalDependencies(this.dataByFiles[filePath.file]);

        // reset ast after analysing each file
        this.ast = null;
      }

    }).bind(this));
    // analyse dependencies globally
    if (analyse) {
      this.analyseDependencies();
    }
  }

  async countLines(path: { file: fs.PathLike }): Promise<number> {
    return new Promise((resolve, reject) => {
      let linesCount = 0;
      var rl = readline.createInterface({
        input: fs.createReadStream(path.file),
        // eslint-disable-next-line no-undef
        output: process.stdout,
        terminal: false
      });
      rl.on('line', function () {
        linesCount++; // on each linebreak, add +1 to 'linesCount'
      });
      rl.on('close', function () {
        resolve(linesCount);
      });
    });
  }
}

export default CodeAnalyser;
