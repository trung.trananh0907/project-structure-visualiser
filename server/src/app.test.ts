import { analyse } from './app';
const databaseName: string = 'dbtest1';

const projectFolder = '../test-cases/';
const fileExtensions = ['.js', '.jsx', '.ts', '.tsx'];
const ignoreFiles = ['node_modules', '.circleci', '.github', '.eslintrc.js', '.git', '.yarn', '.babelrc.js', 'build'];

describe('The tool must be able to store and query the dependencies graph', () => {
  test('The tool must be able to store and query the dependencies graph', async () => {

    // should 
    let dataIsStored = false;
    try {
      await analyse(true, true);
      dataIsStored = true;
    } catch {
      dataIsStored = false;
    }

    expect(dataIsStored).toBeTruthy();
  })
})
