# project-structure-visualiser
This project aims to demonstrate dependencies analysis for React projects. 

## Configurate local environment
### 1. To begin with this project, please clone the project to your local environment

```
  git clone https://gitlab.com/trung.trananh0907/project-structure-visualiser.git
```
***NOTE***: please ensure that you have already installed [Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git) to your local machine

### 2. Install Nodejs using [nvm](https://github.com/nvm-sh/nvm)
On the other hand, nvm can be installed for Windows from [nvm-windows](https://github.com/coreybutler/nvm-windows)
You can also follow the instructions from the links to install Nodejs after successfully installed nvm
For your references, the current Nodejs version to develop this project is v14.17.6

## Setup Neo4j Community Database

Neo4j Community Database is an essential component of this project since it stores and manage dependencies data. The steps to setup Neo4j Community Database in the local environment are as below:

### 3. Download and Install [Neo4j Desktop](https://neo4j.com/download-center/#desktop)
### 4. After opening Neo4j Desktop, create a new local DBMS
![](/instructions/create_local_DBMS.PNG)
### 5. Set ```123``` as password and finish creating the DBMS
![](/instructions/set_password.PNG)
### 6. Start the new DBMS
![](/instructions/start_DBMS.PNG)
***NOTE***: since Neo4j Desktop is for testing purpose only, it is not stable an sometimes take longer than usual to start up. To ensure its up and running, frequently check the DBMS status
![](/instructions/DBMS_status.PNG)
### 7. Create new database
![](/instructions/create_new_database.PNG)
### 8. Input database name: ```dependencies```
***NOTE***: the database must be up and running to allow server stores and queries data

## Setup Nodejs Server
### 9. Navigate to server folder in this project and use command line interface (CLI) to install application level dependencies by using the following script
```
npm install
```
### 10. Start the server by using the following script
```
npm start
```
***NOTE***: the above scripts must be run inside the server folder

## Setup User Interface
### 11. Navigate to client folder in this project and use command line interface (CLI) to install application level dependencies by using the following script
```
npm install
```
### 12. Start the User Interface by using the following script
```
npm start
```
***NOTE***: the above scripts must be run inside the client folder
### 13. After ensuring that the components above are all running, you can navigate to https://localhost:3000 to test the dependencies graph
